extends Node2D

export var max_food_count = 1
export var base_speed = 256
export var rewind_max_time = 3

# Can only rewind at or below this speed
export var rewind_max_speed_factor = 1

var food_count = 0
var speed_factor = 1
var rewind_time = 0
var target_food:Node

func _ready():
	$AnimationPlayer.play("move")

func _process(delta):
	if !target_food or target_food.is_eaten():
		target_food = find_food()
		if target_food:
			add_path_to(target_food.global_position)
		else:
			speed_factor = 0

	if speed_factor < 0:
		rewind_time = min(rewind_time + delta, rewind_max_time)
		speed_factor = (-1 + (rewind_time/rewind_max_time))
		if speed_factor == 0:
			fast_forward()
	elif speed_factor > 1:
		rewind_time = max(0, rewind_time - delta)
		speed_factor = rewind_time+1

	$AnimationPlayer.playback_speed = speed_factor if speed_factor != 0 else 1
	var speed = base_speed * speed_factor
	var pathfollow = $Path2D/PathFollow2D
	pathfollow.offset += speed*delta
	if pathfollow.offset < 0:
		fast_forward()

func rewind():
	if speed_factor >= 1 and speed_factor <= rewind_max_speed_factor:
		speed_factor = -1

func fast_forward():
	if speed_factor <= 0:
		speed_factor = rewind_time+1

func kill():
	if $AnimationPlayer.current_animation != "die":
		$AnimationPlayer.play("die")

func _on_PhysicsBody2D_mouse_entered():
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		rewind()

func _on_PhysicsBody2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				rewind()
			else:
				fast_forward()

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if !event.pressed:
				fast_forward()

func _on_PhysicsBody2D_body_entered(body):
	if body.is_in_group("Hazard"):
		kill()
	elif body.is_in_group("Food"):
		call_deferred("eat", body)

func eat(food):
	if food_count >= max_food_count:
		return
	if food.is_eaten():
		return
	food_count += 1
	food.on_eaten($Path2D/PathFollow2D/Stomach)
	if target_food == food:
		target_food = null

func _on_AnimationPlayer_animation_started(anim_name):
	print("animation_started", anim_name)

func _on_AnimationPlayer_animation_finished(anim_name):
	print("animation_finished", anim_name)
	if anim_name == "die":
		if $AnimationPlayer.playback_speed < 0:
			$AnimationPlayer.play("move", -1, speed_factor, true)
		else:
			queue_free()

func find_path_to(destination):
	var navigation2d = get_parent() as Navigation2D
	if !navigation2d:
		return null
	var path = $Path2D
	var pathfollow = $Path2D/PathFollow2D
	var pos
	var existingpointcount = path.curve.get_point_count()
	if existingpointcount > 0:
		pos = path.curve.get_point_position(existingpointcount-1)
		pos = global_transform.xform(pos)
	else:
		pos = pathfollow.global_position
	return navigation2d.get_simple_path(pos, destination)

func add_path_to(destination):
	var simple_path = find_path_to(destination)
	if !simple_path:
		return
	var path = $Path2D
	var pathfollow = $Path2D/PathFollow2D
	var existingpointcount = path.curve.get_point_count()
	for point in simple_path:
		path.curve.add_point(global_transform.xform_inv(point))
	path.update()
	if existingpointcount == 0:
		pathfollow.offset = 0

func find_food():
	if food_count >= max_food_count:
		return null
	var foods = get_tree().get_nodes_in_group("Food")
	if foods.size() == 0:
		return null
	var pathfollow = $Path2D/PathFollow2D
	var pos = pathfollow.global_position
	var closest_food_dsq = INF
	var closest_food = null
	for food in foods:
		if food.is_eaten():
			continue
		var foodpos = food.global_position
		var path = find_path_to(foodpos)
		if !path:
			continue
		var dsq = 0
		for i in range(1, path.size()):
			dsq += (path[i] - path[i-1]).length_squared()
		if dsq < closest_food_dsq:
			closest_food_dsq = dsq
			closest_food = food
	return closest_food
