extends RigidBody2D

func is_eaten():
	return collision_layer == 2

func on_eaten(newparent):
	if newparent:
		get_parent().remove_child(self)
		newparent.add_child(self)
	position = Vector2()
	collision_layer = 2
	collision_mask = 2
	$Polygon2D.color.a = .5